import { GetBookPage } from './app.po';

describe('get-book App', () => {
  let page: GetBookPage;

  beforeEach(() => {
    page = new GetBookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

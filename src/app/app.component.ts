import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	loadedFeature = 'book'

  ngOnInit(){
  	firebase.initializeApp({
  	apiKey: "AIzaSyDRzSG6rCO8YSj6g3LyMGYon-E212_I3r8",
    authDomain: "getbook-63528.firebaseapp.com",
  	})
  }
  
  onNavigate(feature:string){
  	this.loadedFeature = feature; 

  }
}

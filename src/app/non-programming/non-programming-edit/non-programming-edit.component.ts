import { Component, 
  OnInit,
  ViewChild,
  Output } from '@angular/core';
import {Ingredient} from '../../shared/ingredient.model';
import {BookListService} from '../book-list.service';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
@Component({
  selector: 'app-non-programming-edit',
  templateUrl: './non-programming-edit.component.html',
  styleUrls: ['./non-programming-edit.component.css']
})
export class NonProgrammingEditComponent implements OnInit {
  @ViewChild('f') blForm: NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;


  constructor(private blService: BookListService) { }

  ngOnInit() {
    this.blService.startedEditing
    .subscribe(
      (index:number)=>{
        this.editedItemIndex = index;
        this.editMode = true;
        this.editedItem = this.blService.getIngredient(index);
        this.blForm.setValue({
          name: this.editedItem.name,
          amount:this.editedItem.amount
        })
      }
     );
  }
  onSubmit(form: NgForm){
    const value = form.value;
  	const newIngredient = new Ingredient(value.name,value.amount);
    if (this.editMode) {
      this.blService.updateIngredient(this.editedItemIndex, newIngredient)
    } else{
      this.blService.addIngredient(newIngredient);
    }
    this.editMode = false;
    form.reset();

  }

  onClear(){
    this.blForm.reset()
    this.editMode = false;
  }

  onDelete(){
   
   this.blService.deleteIngredient(this.editedItemIndex);
   this.onClear();
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {BookListService} from './book-list.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-non-programming',
  templateUrl: './non-programming.component.html',
  styleUrls: ['./non-programming.component.css']
})
export class NonProgrammingComponent implements OnInit, OnDestroy {
	ingredients:Ingredient[];
  private subscription: Subscription;

  constructor(private blService: BookListService) { }

  ngOnInit() {
    this.ingredients = this.blService.getIngredients(); //book-list.service
    this.subscription = this.blService.ingredientsChanged
     .subscribe(
       (ingredients: Ingredient[]) =>{
         this.ingredients = ingredients;
       })
  }

onEditItem(index:number){
  this.blService.startedEditing.next(index);

}


ngOnDestroy(){
  this.subscription.unsubscribe();
}
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import {DataStorageService} from './shared/data-storage.service';
import {BookService} from './programming/book.service';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ProgrammingComponent } from './programming/programming.component';
import { NonProgrammingComponent } from './non-programming/non-programming.component';
import { BookDetailComponent } from './programming/book-detail/book-detail.component';
import { BookListComponent } from './programming/book-list/book-list.component';
import { BookItemComponent } from './programming/book-list/book-item/book-item.component';
import { NonProgrammingEditComponent } from './non-programming/non-programming-edit/non-programming-edit.component';
import {DropDownDirective} from './shared/dropdown.directive';
import {BookListService} from './non-programming/book-list.service';
import {AppRoutingModule} from './app.routing.module';
import { StartBookComponent } from './programming/start-book/start-book.component';
import { BookEditComponent } from './programming/book-edit/book-edit.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import {AuthService} from './auth/auth.service';
import {AuthGuard} from './auth/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProgrammingComponent,
    NonProgrammingComponent,
    BookDetailComponent,
    BookListComponent,
    BookItemComponent,
    NonProgrammingEditComponent,
    DropDownDirective,
    StartBookComponent,
    BookEditComponent,
    SignupComponent,
    SigninComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [BookListService,BookService, DataStorageService,AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';



import { ProgrammingComponent } from './programming/programming.component';
import { NonProgrammingComponent } from './non-programming/non-programming.component';
import { StartBookComponent } from './programming/start-book/start-book.component';
import { BookDetailComponent } from './programming/book-detail/book-detail.component';
import { BookEditComponent } from './programming/book-edit/book-edit.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import {AuthGuard} from './auth/auth-guard.service';

const appRoutes: Routes=[
{path:'', redirectTo:'/book', pathMatch:'full'},
{path:'book', component:ProgrammingComponent, children:[
{path:'', component:StartBookComponent },
{path: 'new', component:BookEditComponent, canActivate: [AuthGuard]},
{path:':id', component:BookDetailComponent},
{path: ':id/edit', component:BookEditComponent, canActivate: [AuthGuard]},
]},
{path:'book-list', component:NonProgrammingComponent},
{path:'signup', component:SignupComponent },
{path:'signin', component:SigninComponent }

];

@NgModule({
	imports:[RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})

export class AppRoutingModule{

}
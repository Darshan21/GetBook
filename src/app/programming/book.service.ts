import {Book} from './pbook.model';
import { Subject } from 'rxjs/Subject';
import {Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {BookListService} from '../non-programming/book-list.service';

@Injectable()

export class BookService{
booksChanged = new Subject<Book[]>();

  private	books: Book[] = [
	new Book('A Test Book',
		'programming book',
		'assets/image/pbook/book.jpeg',
		[
         new Ingredient('linux', 99),
         new Ingredient('OS', 99)
		]
	),
	new Book('A Another Test Book',
		'programming book',
		'assets/image/pbook/book.jpeg',
		[
        new Ingredient('linux', 99),
        new Ingredient('linux', 99)
		]
	),
	new Book('Third Test Book',
		'programming book',
		'assets/image/pbook/book.jpeg',
		[
        new Ingredient('linux', 99),
        new Ingredient('linux', 99)
		]
	)];


constructor(private blService:BookListService){}

setBooks(books:Book[]){
	this.books  = books;
	this.booksChanged.next(this.books.slice());

}

getBook(index:number){
	return this.books[index];
}

getBooks() {    //getBooks is a copy of array(books) and its called from book-list.component
	return this.books.slice();
}

addIngredientsToBookList(ingredients:Ingredient[]){
	this.blService.addIngredients(ingredients);

}

addBook(book:Book){
	this.books.push(book);
    this.booksChanged.next(this.books.slice());
}

updateBook(index: number, newBook: Book){
	this.books[index] = newBook;
	this.booksChanged.next(this.books.slice());

}

deleteBook(index: number){
	this.books.splice(index,1);
	this.booksChanged.next(this.books.slice());
}

}

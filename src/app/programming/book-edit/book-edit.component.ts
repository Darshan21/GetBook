import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params, Router} from '@angular/router'
import {FormGroup,FormArray,FormControl,Validators } from '@angular/forms';

import { BookService } from '../book.service';
import {Book} from '../pbook.model';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {
	id:number;
	editMode = false;
  bookForm: FormGroup;

  constructor(private route:ActivatedRoute,
              private bookService:BookService,
              private router: Router) { }

  ngOnInit() {
    console.log("ngonInit in book - edit")
  	this.route.params
  	.subscribe((params:Params) => {
  		this.id = +params['id'];
  		this.editMode = params['id'] != null;
      this.initForm();
  	});

}

onSubmit(){
  // const newBook = new Book(
  //   this.bookForm.value['name'],
  //   this.bookForm.value['description'],
  //   this.bookForm.value['imagePath'],
  //   this.bookForm.value['ingredients']);
     console.log("submitted the new book")
     if(this.editMode){
       this.bookService.updateBook(this.id, this.bookForm.value)
     } else {
       this.bookService.addBook(this.bookForm.value);
     }
     this.onCancel();
    }

onAddIngredient(){
  (<FormArray>this.bookForm.get('ingredients')).push(
    new FormGroup({
      'name': new FormControl(null, Validators.required),
      'amount': new FormControl(null, [
       Validators.required,
       Validators.pattern(/^[1-9]+[0-9]*$/)
        ])
    })
  );
}

onCancel(){
this.router.navigate(['../'],{relativeTo:this.route});
}

onDeleteIngredient(index:number){
  (<FormArray>this.bookForm.get('ingredients')).removeAt(index);
}

 private initForm(){
  let bookName = '';
  let bookImagePath = '';
  let bookDescription = '';
  let bookIngredients = new FormArray([]);

  if(this.editMode){
    const book = this.bookService.getBook(this.id);
    bookName = book.name;
    bookImagePath = book.imagePath;
    bookDescription = book.description;
    if(book['ingredients']){
      for(let ingredient of book.ingredients){
        bookIngredients.push(
          new FormGroup({
            'name': new FormControl(ingredient.name,Validators.required),
            'amount': new FormControl(ingredient.amount, [ 
            Validators.required,
            Validators.pattern(/^[1-9]+[0-9]*$/)
            ])
          })
         );
      }
    }
   }

   this.bookForm = new FormGroup({
     'name': new FormControl(bookName,Validators.required),
     'imagePath': new FormControl(bookImagePath,Validators.required),
     'description': new FormControl(bookDescription,Validators.required),
     'ingredients': bookIngredients  // form array...
   })

 
 }
}
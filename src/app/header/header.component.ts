import { Component } from '@angular/core';
import {Response} from '@angular/http';

import {DataStorageService} from '../shared/data-storage.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private dataStorageService: DataStorageService,
              private authService: AuthService) { }

  onSaveData(){
  	console.log("header-onSaveData"),
  	this.dataStorageService.storeBooks()
  	.subscribe(
  		(response: Response) => {
  		console.log(response);
  	}
  	);
  }

  onLogout(){
    this.authService.logout();
  }

  onFetchData(){
  	console.log("header-onFetchData")
  	this.dataStorageService.getBooks()
  	
  }

}

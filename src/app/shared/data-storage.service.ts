import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http'
import {BookService} from '../programming/book.service';
import {Book} from '../programming/pbook.model';
import 'rxjs/Rx';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class DataStorageService{
	constructor(private http:Http, 
		        private bookService: BookService,
		        private authService:AuthService){}

	storeBooks(){
		const token = this.authService.getToken()
		console.log("put request...");
		return this.http.put('https://getbook-63528.firebaseio.com/books.json?auth='+token,this.bookService.getBooks());
	}

	getBooks(){
		const token = this.authService.getToken()

		console.log("get called...");
		return this.http.get('https://getbook-63528.firebaseio.com/books.json?auth=' + token)
		.map(
			(response:Response) => {
				const books: Book[] = response.json();
				for(let book of books){
					if(!book['ingredients']){
						book['ingredients']=[];
					}
					
				}
                
				return books;

			})
		.subscribe(
  		(books: Book[]) => {
  			
  			this.bookService.setBooks(books);
  		});
	}
}